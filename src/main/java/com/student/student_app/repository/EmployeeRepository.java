package com.student.student_app.repository;
import com.student.student_app.data.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {

    public List<Employee> findByDepartment(Integer department);
    @Query("select count(*) from Employee as e where e.department=?1")
    public Integer totalEmployeesInDepartment(Integer department);
    public List<Employee> findByFirstNameStartingWith(String firstName);
    public List<Employee> findByLastNameEndingWith(String lastName);
    public List<Employee> findByFirstName(String firstName);
    @Query (nativeQuery = true,value="select id,department,email,firstName,lastName,phoneNumber,salary" +
            " from Employee as e where e.salary>=?1 and e.salary<=?2 limit " +
            "?3")
    public List<List<String>> employeeSalaryRange(double startSalary, double endSalary,int n);
    @Query ("select department,MAX(salary) from Employee as e group by e.department ")
    public List<List<String>> maxSalaryDepartmentWise();

}
