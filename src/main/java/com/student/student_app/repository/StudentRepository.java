package com.student.student_app.repository;

import com.student.student_app.data.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Integer> {

    public List<Student> findByDepartment(Integer department);
    @Query("select count(*) from Student as s where s.department=?1")
    public Integer totalStudentsInDepartment(Integer department);
    public List<Student> findByFirstNameStartingWith(String firstName);
    public List<Student> findByLastNameEndingWith(String lastName);
    public List<Student> findByLastNameAndFirstName(String lastName,String firstName);
}
