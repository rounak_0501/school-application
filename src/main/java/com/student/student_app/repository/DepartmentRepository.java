package com.student.student_app.repository;

import com.student.student_app.data.models.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DepartmentRepository extends JpaRepository<Department,Integer> {

    public Optional<Department> findByDepartmentName(String department);
}
