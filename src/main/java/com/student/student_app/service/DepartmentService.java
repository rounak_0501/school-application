package com.student.student_app.service;

import com.student.student_app.data.models.Department;
import com.student.student_app.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {

    @Autowired
    DepartmentRepository departmentRepository;

    /*
    add new Employee to database
     */
    public String createDepartment(Department departmentRequest) {
        Department newDepartment = new Department();
        newDepartment.setId(departmentRequest.getId());
        newDepartment.setDepartmentName(departmentRequest.getDepartmentName());
        departmentRepository.save(newDepartment);
        return "New Department created successfully";
    }


    /*
    Get employee by id
     */
    public Optional<Department> getASingleDepartment(Integer departmentId){
        Optional<Department> department=departmentRepository.findById(departmentId);
        return department;
    }


    /*
    List of all employees
     */
    public List<Department> getAllDepartment() {
        return departmentRepository.findAll();
    }


    /*
    remove employee from organization
     */
    public void removeDepartment(Integer departmentId){
        departmentRepository.deleteById(departmentId);
    }


    /*
    Update Employee details
     */
    public String updateEmployee(Integer departmentId, Department departmentRequest){
        Optional<Department> employee = departmentRepository.findById(departmentId);
        employee.get().setId(departmentRequest.getId());
        employee.get().setDepartmentName(departmentRequest.getDepartmentName());
        departmentRepository.save(employee.get());
        return "Updated successfully";
    }
}
