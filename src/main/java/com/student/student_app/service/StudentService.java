package com.student.student_app.service;
import com.student.student_app.data.models.Student;
import com.student.student_app.data.models.Department;
import com.student.student_app.data.payloads.Payload;
import com.student.student_app.repository.DepartmentRepository;
import com.student.student_app.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private DepartmentRepository departmentRepository;


    /*
    add new Employee to database
     */
    public String createStudent(Payload studentRequest) {
        Student newStudent = new Student();
        newStudent.setUid(studentRequest.getUid());
        newStudent.setFirstName(studentRequest.getFirstName());
        newStudent.setLastName(studentRequest.getLastName());
        newStudent.setPhoneNumber(studentRequest.getPhoneNumber());
        newStudent.setEmail(studentRequest.getEmail());
        Optional<Department> department=departmentRepository.findByDepartmentName(studentRequest.getDepartment());
        if(department.isEmpty()){
            Department newDepartment=new Department();
            newDepartment.setDepartmentName(studentRequest.getDepartment());
            departmentRepository.save(newDepartment);
            department=departmentRepository.findByDepartmentName(studentRequest.getDepartment());
        }
        newStudent.setDepartment(department.get().getId());
        studentRepository.save(newStudent);
        return "New Student created successfully";
    }


    /*
    Get employee by id
     */
    public Payload getASingleStudent(Integer studentId){
        Optional<Student> student=studentRepository.findById(studentId);
        if(student.isEmpty())return new Payload();
        return studentResponse(student.get());
    }


    /*
    List of all employees
     */
    public List<Payload> getAllStudents() {
        List<Student> students= studentRepository.findAll();
        if(students.isEmpty())return new ArrayList<Payload>();
        List<Payload> studentResponse=new ArrayList<>();
        for(Student student:students){
            studentResponse.add(studentResponse(student));
        }
        return studentResponse;
    }


    /*
    remove employee from organization
     */
    public String removeStudent(Integer studentId){
        Optional<Student> student=studentRepository.findById(studentId);
        if(student.isEmpty())
            return "Employee not found";
        studentRepository.deleteById(studentId);
        return "Data deleted successfully";
    }


    /*
    Update Employee details
     */
    public String updateStudent(Integer studentId, Payload studentRequest){
        Optional<Student> student= studentRepository.findById(studentId);
        student.get().setFirstName(studentRequest.getFirstName());
        student.get().setLastName(studentRequest.getLastName());
        student.get().setPhoneNumber(studentRequest.getPhoneNumber());
        student.get().setEmail(studentRequest.getEmail());
        Optional<Department> department=departmentRepository.findByDepartmentName(studentRequest.getDepartment());
        if(department.isEmpty()){
            Department newDepartment=new Department();
            newDepartment.setDepartmentName(studentRequest.getDepartment());
            departmentRepository.save(newDepartment);
            department=departmentRepository.findByDepartmentName(studentRequest.getDepartment());
        }
        student.get().setDepartment(department.get().getId());
        studentRepository.save(student.get());
        return "Updated successfully";
    }
    /*
    Get all students in a department
     */
    public List<Payload> getStudentsInDepartment(String department) {
        Optional<Department> departmentId=departmentRepository.findByDepartmentName(department);
        if(departmentId.isEmpty())return new ArrayList<Payload>();
        List<Student> students= studentRepository.findByDepartment(departmentId.get().getId());
        if(students.isEmpty())return new ArrayList<Payload>();
        List<Payload> studentResponse=new ArrayList<>();
        for(Student student:students){
            studentResponse.add(studentResponse(student));
        }
        return studentResponse;
    }
    /*
    Get total no of students in department
     */
    public Integer getNoOfStudentsInDepartment(String department) {
        Optional<Department> departmentId=departmentRepository.findByDepartmentName(department);
        if(departmentId.isEmpty())return 0;
        return studentRepository.totalStudentsInDepartment(departmentId.get().getId());
    }
    /*
    find by starting first name
     */
    public List<Payload> findByPrefix(String prefix){
        List<Student> students=studentRepository.findByFirstNameStartingWith(prefix);
        List<Payload> studentResponse=new ArrayList<>();
        for(Student student:students){
            studentResponse.add(studentResponse(student));
        }
        return studentResponse;
    }
    /*
    find by ending last name
     */
    public List<Payload> findBySufix(String sufix){
        List<Student> students=studentRepository.findByLastNameEndingWith(sufix);
        List<Payload> studentResponse=new ArrayList<>();
        for(Student student:students){
            studentResponse.add(studentResponse(student));
        }
        return studentResponse;
    }
    /*
    find by exact match
     */
    public List<Payload> findByName(String name){
        String firstName=name.substring(0,name.indexOf(' ')),lastName=name.substring(name.indexOf(' ')+1);
        List<Student> students=studentRepository.findByLastNameAndFirstName(lastName,firstName);
        List<Payload> studentResponse=new ArrayList<>();
        for(Student student:students){
            studentResponse.add(studentResponse(student));
        }
        return studentResponse;
    }
    /*
    student to student response
     */
    private Payload studentResponse(Student student){
        Payload response=new Payload();
        response.setUid(student.getUid());
        response.setEmail(student.getEmail());
        response.setPhoneNumber(student.getPhoneNumber());
        response.setFirstName(student.getFirstName());
        response.setLastName(student.getLastName());
        response.setDepartment(departmentRepository.findById(student.getDepartment()).get().getDepartmentName());
        return response;
    }
}
