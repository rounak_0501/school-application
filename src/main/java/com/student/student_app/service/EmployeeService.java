package com.student.student_app.service;

import com.student.student_app.data.payloads.EmployeeRequest;
import com.student.student_app.data.models.Department;
import com.student.student_app.data.models.Employee;
import com.student.student_app.repository.DepartmentRepository;
import com.student.student_app.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private DepartmentRepository departmentRepository;


    /*
    add new Employee to database
     */
    public String createEmployee(EmployeeRequest employeeRequest) {
        Employee newEmployee = new Employee();
        newEmployee.setId(employeeRequest.getUid());
        newEmployee.setFirstName(employeeRequest.getFirstName());
        newEmployee.setLastName(employeeRequest.getLastName());
        newEmployee.setPhoneNumber(employeeRequest.getPhoneNumber());
        newEmployee.setEmail(employeeRequest.getEmail());
        newEmployee.setSalary(employeeRequest.getSalary());
        Optional<Department> department=departmentRepository.findByDepartmentName(employeeRequest.getDepartment());

        if(department.isEmpty()){
            Department newDepartment=new Department();
            newDepartment.setId(newDepartment.getId());
            newDepartment.setDepartmentName(employeeRequest.getDepartment());
            departmentRepository.save(newDepartment);
            department=departmentRepository.findByDepartmentName(employeeRequest.getDepartment());
        }
        newEmployee.setDepartment(department.get().getId());
        employeeRepository.save(newEmployee);
        return "New Employee created successfully";
    }
    /*
    Get all employees in a department
     */
    public List<EmployeeRequest> getEmployeesInDepartment(String department) {
        Optional<Department> departmentId=departmentRepository.findByDepartmentName(department);

        if(departmentId.isEmpty())return new ArrayList<EmployeeRequest>();
        List<Employee> employees= employeeRepository.findByDepartment(departmentId.get().getId());
        if(employees.isEmpty())return new ArrayList<EmployeeRequest>();
        List<EmployeeRequest> employeeResponse=new ArrayList<>();
        for(Employee employee:employees){
            employeeResponse.add(employeeResponse(employee));
        }
        return employeeResponse;
    }

    /*
    Get employee by id
     */
    public EmployeeRequest getASingleEmployee(Integer employeeId){
        Optional<Employee> employee=employeeRepository.findById(employeeId);
        return employeeResponse(employee.get());
    }


    /*
    List of all employees
     */
    public List<EmployeeRequest> getAllEmployee() {
        List<Employee> employees= employeeRepository.findAll();
        List<EmployeeRequest> employeeResponse=new ArrayList<>();
        for(Employee employee:employees){
            employeeResponse.add(employeeResponse(employee));
        }
        return employeeResponse;
    }


    /*
    remove employee from organization
     */
    public String removeEmployee(Integer employeeId){
        Optional<Employee> employee=employeeRepository.findById(employeeId);
        if(employee.isEmpty())
            return "Employee not found";
        employeeRepository.deleteById(employeeId);
        return "Data deleted successfully";
    }


    /*
    Update Employee details
     */
    public String updateEmployee(Integer employeeId, EmployeeRequest employeeRequest){
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        employee.get().setFirstName(employeeRequest.getFirstName());
        employee.get().setLastName(employeeRequest.getLastName());
        employee.get().setPhoneNumber(employeeRequest.getPhoneNumber());
        employee.get().setEmail(employeeRequest.getEmail());
        employee.get().setSalary(employeeRequest.getSalary());
        Optional<Department> department=departmentRepository.findByDepartmentName(employeeRequest.getDepartment());
        if(department.isEmpty()){
            Department newDepartment=new Department();
            newDepartment.setId(newDepartment.getId());
            newDepartment.setDepartmentName(employeeRequest.getDepartment());
            departmentRepository.save(newDepartment);
            department=departmentRepository.findByDepartmentName(employeeRequest.getDepartment());
        }
        employee.get().setDepartment(department.get().getId());
        employeeRepository.save(employee.get());
        return "Updated successfully";
    }
    /*
    Get total no of students in department
     */
    public Integer getNoOfStudentsInDepartment(String department) {
        Optional<Department> departmentId=departmentRepository.findByDepartmentName(department);
        if(departmentId.isEmpty())return 0;
        return employeeRepository.totalEmployeesInDepartment(departmentId.get().getId());
    }
    /*
    find by starting first name
     */
    public List<EmployeeRequest> findByPrefix(String prefix){
        List<Employee> employees=employeeRepository.findByFirstNameStartingWith(prefix);
        List<EmployeeRequest> employeeResponse=new ArrayList<>();
        for(Employee employee:employees){
            employeeResponse.add(employeeResponse(employee));
        }
        return employeeResponse;
    }
    /*
    find by ending last name
     */
    public List<EmployeeRequest> findBySufix(String sufix){
        List<Employee> employees=employeeRepository.findByLastNameEndingWith(sufix);
        List<EmployeeRequest> employeeResponse=new ArrayList<>();
        for(Employee employee:employees){
            employeeResponse.add(employeeResponse(employee));
        }
        return employeeResponse;
    }
    /*
    find by exact match
     */
    public List<EmployeeRequest> findByName(String name){
        //String firstName=name.substring(0,name.indexOf(' ')),lastName=name.substring(name.indexOf(' ')+1);
        List<Employee> employees=employeeRepository.findByFirstName(name);
        List<EmployeeRequest> employeeResponse=new ArrayList<>();
        for(Employee employee:employees){
            employeeResponse.add(employeeResponse(employee));
        }
        return employeeResponse;
    }
    /*
    employees in given salary range
     */
    public List<String> employeeInSalaryRange(double start,double end,int n){
        List<List<String>> employees=employeeRepository.employeeSalaryRange(start,end,n);
        if(employees.size()<n) n=employees.size();
        List<String> employeeResponse=new ArrayList<>();
        for(int i=0;i<n;i++){
            String detail="";
            for(int j=0;j<employees.get(i).size();j++)
                detail+=employees.get(i).get(j)+" ";
            employeeResponse.add(detail);
        }
        return employeeResponse;
    }
    /*
    max salary in each department
     */
    public List<String> maxSalaryInEachDepartment(){
        List<List<String>> employees=employeeRepository.maxSalaryDepartmentWise();
        List<String> employeeResponse=new ArrayList<>();
        for(List<String> employee:employees){
            employeeResponse.add(employee.get(0)+" "+employee.get(1));
        }
        return employeeResponse;
    }
    /*
    employee to employee response
     */
    private EmployeeRequest employeeResponse(Employee employee){
        EmployeeRequest response=EmployeeRequest.builder()
        .uid(employee.getId())
        .email(employee.getEmail())
        .phoneNumber(employee.getPhoneNumber())
        .firstName(employee.getFirstName()).lastName(employee.getLastName())
        .department(departmentRepository.findById(employee.getDepartment()).get().getDepartmentName())
                .salary(employee.getSalary()).build();
        return response;
    }
}
