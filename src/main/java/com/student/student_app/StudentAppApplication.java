package com.student.student_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class StudentAppApplication {

	@RequestMapping(value="/home",method=RequestMethod.GET)
	public String landingPage(){return "Do \n /student for student info \n/staff for staff info\n/department for department info";}
	public static void main(String[] args) {
		SpringApplication.run(StudentAppApplication.class, args);
	}

}
