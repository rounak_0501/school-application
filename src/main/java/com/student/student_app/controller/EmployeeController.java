package com.student.student_app.controller;

import com.student.student_app.data.models.Employee;
import com.student.student_app.data.payloads.Payload;
import com.student.student_app.service.EmployeeService;
import com.student.student_app.data.payloads.EmployeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/all")
    public ResponseEntity<List> getAllEmployees () {
        List<EmployeeRequest> employees=employeeService.getAllEmployee();
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }
    @GetMapping("/find/{id}")
    public ResponseEntity<EmployeeRequest> getEmployeeById(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(employeeService.getASingleEmployee(id), HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<String> addEmployee( @RequestBody EmployeeRequest employee) {
        String message=employeeService.createEmployee(employee);
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateEmployee( @PathVariable Integer id, @RequestBody EmployeeRequest employee) {
        String message=employeeService.updateEmployee(id,employee);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteEmployee(@PathVariable("id") Integer id) {
        String message=employeeService.removeEmployee(id);
        return new ResponseEntity<>(message,HttpStatus.OK);
    }
    @GetMapping("/find/employees-{department}")
    public ResponseEntity<List<EmployeeRequest>> getEmployeesInDepartment(@PathVariable("department") String department) {
        return new ResponseEntity<>(employeeService.getEmployeesInDepartment(department), HttpStatus.OK);
    }
    @GetMapping("/find/totalEmployees/{department}")
    public ResponseEntity<Integer> getByDepartment(@PathVariable("department") String department) {
        return new ResponseEntity<>(employeeService.getNoOfStudentsInDepartment(department), HttpStatus.OK);
    }
    @GetMapping("/find/name/{name}")
    public ResponseEntity<List<EmployeeRequest>> getByName(@PathVariable("name") String name) {
        return new ResponseEntity<>(employeeService.findByName(name), HttpStatus.OK);
    }
    @GetMapping("/find/prefix-name/{name}")
    public ResponseEntity<List<EmployeeRequest>> getByPrefixName(@PathVariable("name") String name) {
        return new ResponseEntity<>(employeeService.findByPrefix(name), HttpStatus.OK);
    }
    @GetMapping("/find/sufix-name/{name}")
    public ResponseEntity<List<EmployeeRequest>> getBySufixName(@PathVariable("name") String name) {
        return new ResponseEntity<>(employeeService.findBySufix(name), HttpStatus.OK);
    }
    @GetMapping("/find/{noOfEmployees}insalaryInRange/{start}-{end}")
    public ResponseEntity<List<String>> employeeInSalaryRange(@PathVariable("start")double start,
                                                                       @PathVariable("end")double end,
                                                                       @PathVariable("noOfEmployees")int n) {
        return new ResponseEntity<>(employeeService.employeeInSalaryRange(start,end,n), HttpStatus.OK);
    }
    @GetMapping("/find/salary/maxSalary")
    public ResponseEntity<List<String>> maxSalaryDepartmentWise() {
        return new ResponseEntity<>(employeeService.maxSalaryInEachDepartment(), HttpStatus.OK);
    }
}
