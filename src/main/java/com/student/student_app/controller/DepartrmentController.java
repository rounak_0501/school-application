package com.student.student_app.controller;

import com.student.student_app.service.DepartmentService;
import com.student.student_app.data.models.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/department")
public class DepartrmentController {

    @Autowired
    DepartmentService departmentService;

    @GetMapping("/all")
    public ResponseEntity<List> getAllDepartment () {
        List<Department> departments=departmentService.getAllDepartment();
        return new ResponseEntity<>(departments, HttpStatus.OK);
    }
    @GetMapping("/find/{id}")
    public ResponseEntity<Optional<Department>> getDepartmentById (@PathVariable("id") Integer id) {
        return new ResponseEntity<>(departmentService.getASingleDepartment(id), HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<String> addDepartment( @RequestBody Department department) {
        String message=departmentService.createDepartment(department);
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateDepartment( @PathVariable Integer id, @RequestBody Department department) {
        String message=departmentService.updateEmployee(id,department);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteDepartment(@PathVariable("id") Integer id) {
        departmentService.removeDepartment(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
