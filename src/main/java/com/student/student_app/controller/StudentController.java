package com.student.student_app.controller;

import com.student.student_app.data.payloads.Payload;
import com.student.student_app.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/all")
    public ResponseEntity<List> getAllStudents () {
        List<Payload> students=studentService.getAllStudents();
        return new ResponseEntity<>(students, HttpStatus.OK);
    }
    @GetMapping("/find/{id}")
    public ResponseEntity<Payload> getStudentById (@PathVariable("id") Integer id) {
        return new ResponseEntity<>(studentService.getASingleStudent(id), HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<String> addStudent( @RequestBody Payload student) {
        String message=studentService.createStudent(student);
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateStudent( @PathVariable Integer id, @RequestBody Payload student) {
        String message=studentService.updateStudent(id,student);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
    @GetMapping("/find/students/{department}")
    public ResponseEntity<List<Payload>> getByDepartment(@PathVariable("department") String department) {
        return new ResponseEntity<>(studentService.getStudentsInDepartment(department), HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable("id") Integer id) {
        String message=studentService.removeStudent(id);
        return new ResponseEntity<>(message,HttpStatus.OK);
    }
    @GetMapping("/find/totalStudents/{department}")
    public ResponseEntity<Integer> getTotalStudentsByDepartment(@PathVariable("department") String department) {
        return new ResponseEntity<>(studentService.getNoOfStudentsInDepartment(department), HttpStatus.OK);
    }
    @GetMapping("/find/name/{name}")
    public ResponseEntity<List<Payload>> getByName(@PathVariable("name") String name) {
        return new ResponseEntity<>(studentService.findByName(name), HttpStatus.OK);
    }
    @GetMapping("/find/prefix-name/{name}")
    public ResponseEntity<List<Payload>> getByPrefixName(@PathVariable("name") String name) {
        return new ResponseEntity<>(studentService.findByPrefix(name), HttpStatus.OK);
    }
    @GetMapping("/find/sufix-name/{name}")
    public ResponseEntity<List<Payload>> getBySuffixName(@PathVariable("name") String name) {
        return new ResponseEntity<>(studentService.findBySufix(name), HttpStatus.OK);
    }
}
