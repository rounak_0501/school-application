package com.student.student_app.data.payloads;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
public class EmployeeRequest extends Payload {
    private double salary;
    @Builder
    public EmployeeRequest(Integer uid,String email,String phoneNumber,String firstName,
                           String lastName,String department,double salary){
        super(uid,firstName,lastName,phoneNumber,email,department);
        this.salary=salary;
    }
}
