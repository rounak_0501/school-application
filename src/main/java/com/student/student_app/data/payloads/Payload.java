package com.student.student_app.data.payloads;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class Payload {
    @NonNull private Integer uid;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    @NonNull private String department;
    public Payload(){}
}
