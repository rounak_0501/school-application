package com.student.student_app.data.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class Student {
    @NonNull
    @Id
    private Integer uid;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    @NonNull private Integer department;
}
