package com.student.student_app.data.models;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@Entity
@NoArgsConstructor
public class Employee {
        @Id
        private Integer id;
        private String firstName;
        private String lastName;
        private String phoneNumber;
        private String email;
        private double salary;
        @NotNull
        private Integer department;
}


